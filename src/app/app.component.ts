import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/components/common/menuitem';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Evaluator';

  items: MenuItem[];

  ngOnInit() {
    this.items = [
      { label: 'Home', icon: 'fa fa-home', routerLink: '/home' },
      { label: 'Statistics', icon: 'fa fa-bar-chart-o', routerLink: '/statistics' },
      { label: 'Route', icon: 'fa fa-car', routerLink: '/route' }
    ];
  }
}
